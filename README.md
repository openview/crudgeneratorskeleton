CrudGeneratorSkeleton
=====================

Skeleton per il Sensio CrudGenerator, si adatta al tema AceUi (https://wrapbootstrap.com/theme/ace-responsive-admin-template-WB0B30DGR), che va acquistato a parte.


## Installazione

Se non presente nella macchina di sviluppo, clonare questo repository (in questo caso considero di
clonarlo in ~/prj ) in modo da poterlo usare anche in altri progetti.

Spostarsi nel progetto che userà il CrudGenerator
Installare tramite composer il SensioGeneratorBundle.

Andare nella directory app/Resources e creare il collegamento al repository del Bundle

    ln -s ../../../CrudGeneratorSkeleton/ SensioGeneratorBundle

Adesso il SensioGeneratorBundle userà il nostro skeleton anzichè quello di default.

### Generator/DoctrineCrudGenerator.php (in vendor/sensio/generator/...)

Aggiungere ai metodi generateNewView($dir) e generateEditView($dir) la seguente riga, per avere a
disposizione i campi dell'entità e costruire automagicamente il form campo per campo.

    'fields'            => $this->metadata->fieldMappings,


## Uso

Avviare il generatore con un comando del tipo:

    php app/console generate:doctrine:crud --entity="OpenviewUserBundle:Qualcosa" --format="yml" --with-write --route-prefix="/admin/qualcosa"

Effettuare poi le modifiche sotto riportate, e adattare quello che rimane.


## Modifiche da effettuare

Dopo la generazione del CRUD, rimangono alcune modifiche da fare:

### Twig edit e new

- Eliminare i campi non gestiti nel form, come l'ID e i campi oggetto di join.

### Form

- Modificare il type, che al momento è sempre 'text'
- Gestire i campi di tipo array

    
### Copiare i Javascript

Accodare in fondo ad alcuni twig, il loro codice javascript che non sono ancora stato in grado di integrare in twig
in modo agevole.

edit.html.twig:

    {% block javascripts %}
    <script type="text/javascript">
        $(document).ready(function(){
            // chiede conferma prima di procedere con eliminazione elemento
            $('form[name="form"]').submit(function(event){
                //event.preventDefault();
                var c = confirm('{{ 'msg.alert.deleteconfirm' | trans }}');
                return c;
            });
        });
    </script>
    {% endblock %}
    

index.html.twig:

    {% block javascripts %}
        <script type="text/javascript">
        $(document).ready(function() {
            // attiva datatables e ne imposta le colonne ordinabili
            $('.dataTable').each(function(){
                var bFilter = true;
                if($(this).hasClass('nofilter')){
                    bFilter = false;
                }
                var columnSort = new Array; 
                $(this).find('thead tr tH').each(function(){
                    if($(this).attr('data-bSortable') == 'false') {
                        columnSort.push({ "bSortable": false });
                    } else {
                        columnSort.push({ "bSortable": true });
                    }
                });
                $(this).dataTable({
                    "sPaginationType": "full_numbers",
                    "bFilter": bFilter,
                    "fnDrawCallback": function( oSettings ) {
                    },
                    "aoColumns": columnSort,
                    bAutoWidth: false
                });
            });
        } );
        </script>
    {% endblock %}


show.html.twig:

    {% block javascripts %}
    <script type="text/javascript">
        $(document).ready(function(){
            // chiede conferma prima di procedere con eliminazione elemento
            $('form').submit(function(event){
                //event.preventDefault();
                var c = confirm('{{ 'msg.alert.deleteconfirm' | trans }}');
                return c;
            });
        });
    </script>
    {% endblock %}
